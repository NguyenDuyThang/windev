﻿// RClickToLClick.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "RClickToLClick.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_RCLICKTOLCLICK, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_RCLICKTOLCLICK));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_RCLICKTOLCLICK));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_RCLICKTOLCLICK);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
HWND hwnd = NULL;
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   hwnd = CreateWindowEx(0, L"BUTTON", L"test", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 350, 150, 50, 20, hWnd, (HMENU)IDC_BUTTON, hInst, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
bool i = false;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_KEYDOWN:{
						if (GetAsyncKeyState(VK_SHIFT)){

							if (i == false){
								typedef void(*PROCIN)(HWND);
								HINSTANCE hinstLib;
								PROCIN procInstl;

								hinstLib = LoadLibrary(L"RClickToLClickDLL.dll");
								if (hinstLib != NULL){
									procInstl = (PROCIN)GetProcAddress(hinstLib, "installHook");
									if (procInstl != NULL){
										procInstl(hWnd);
									}
								}
								i = true;
							}
							else{
								typedef bool(*PROCUNIN)(HWND);

								HINSTANCE hinstLib;
								PROCUNIN procunInstl;

								hinstLib = LoadLibrary(L"RClickToLClickDLL.dll");
								if (hinstLib != NULL){
									procunInstl = (PROCUNIN)GetProcAddress(hinstLib, "uninstallHook");
									if (procunInstl != NULL){
										procunInstl(hWnd);
									}
								}
								i = false;
							}
						}
						break;
	}
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case WM_CREATE:
			//hwnd = CreateWindow(NULL, L"test", WS_CHILD | WS_VISIBLE, 50, 50, 300, 300, hWnd, NULL, hInst, NULL);
			break;
		case IDC_BUTTON:
			MessageBox(0, L"test", 0, 0);
			break;
		
		case ID_HOOK_INSTALLHOOK:{
									 typedef void(*PROCIN)(HWND);
									 HINSTANCE hinstLib;
									 PROCIN procInstl;

									 hinstLib = LoadLibrary(L"RClickToLClickDLL.dll");
									 if (hinstLib != NULL){
										 procInstl = (PROCIN)GetProcAddress(hinstLib, "installHook");
										 if (procInstl != NULL){
											 procInstl(hWnd);
										 }
									 }
									 break;
		}
		case ID_HOOK_UNINSTALLHOOK:{
									   typedef bool(*PROCUNIN)(HWND);

									   HINSTANCE hinstLib;
									   PROCUNIN procunInstl;

									   hinstLib = LoadLibrary(L"RClickToLClickDLL.dll");
									   if (hinstLib != NULL){
										   procunInstl = (PROCUNIN)GetProcAddress(hinstLib, "uninstallHook");
										   if (procunInstl != NULL){
											   procunInstl(hWnd);
										   }
									   }
									   break;
		}
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
