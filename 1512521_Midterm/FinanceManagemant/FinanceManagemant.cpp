﻿// FinanceManagemant.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "FinanceManagemant.h"
#include <windowsX.h>
#include <winuser.h>
#include <CommCtrl.h>
#include <iostream>
#include <fcntl.h> //_O_WTEXT
#include <io.h>    //_setmode()
#include <string>
#include <locale>
#include <codecvt> //possible C++11?
#include <fstream>
#include <vector>
#include <objidl.h>
#include <gdiplus.h>
#pragma comment(lib, "gdiplus.lib")

using namespace Gdiplus;

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")

using namespace std;

#define MAX_LOADSTRING 100

struct FM{	//FinanceManagement
	WCHAR category[30];
	WCHAR details[1000];
	WCHAR cash[20];
};

HWND cbb;
HWND paint;
HWND eating;
HWND moving;
HWND house;
HWND car;
HWND food;
HWND service;
vector<FM*> fmdata;
Graphics *graphics = NULL;
GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR gdiplusToken;

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK FinanceManagement(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	_setmode(_fileno(stdout), _O_WTEXT); //needed for output
	
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_FINANCEMANAGEMANT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_FINANCEMANAGEMANT));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_FINANCEMANAGEMANT));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_FINANCEMANAGEMANT);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//

void writeData(vector<FM*> data){
	_setmode(_fileno(stdout), _O_WTEXT); //needed for output
	std::locale loc(std::locale(), new std::codecvt_utf8<wchar_t>);  // UTF-8
	std::wofstream fout(L"data.txt");
	if (!fout) {
		std::wcout << L"Không thể tạo file data.txt\n";
	}
	else {
		fout.imbue(loc);
		//data[data.size() - 1]->cash[wcslen(data[data.size() - 1]->cash) - 1] = '\0';
		for (int i = 0; i < data.size(); i++){
			if (i == data.size() - 1)
				fout << data[i]->category << L" - " << data[i]->details << L" - " << data[i]->cash;
			else
				fout << data[i]->category << L" - " << data[i]->details << L" - " << data[i]->cash << L'\n';
		}
	}
	fout.close();
}

vector<FM*> readData(){
	_setmode(_fileno(stdin), _O_WTEXT); //needed for input
	locale loc(std::locale(), new codecvt_utf8<wchar_t>);  // UTF-8

	wifstream fin(L"data.txt");

	vector<FM*> data;
	FM *d = NULL;
	if (!fin) {
		wcout << L"Không thể đọc file data.txt\n";
	}
	else {
		while (!fin.eof()){
			WCHAR t[100];
			d = new FM();
			fin.imbue(loc);
			//getline(fin, (wstring)t);
			while (1){
				fin >> t;
				if (t[0] == '-')
					break;
				wcscat(d->category, t);
				wcscat(d->category, (WCHAR*)" ");
			}

			while (1){
				fin >> t;
				if (t[0] == '-')
					break;
				wcscat(d->details, t);
				wcscat(d->details, L" ");
			}

			fin >> t;
			wcscat(d->cash, t);

			data.push_back(d);
		}
		fin.close();
	}
	return data;
}

long long showSpentMoney(HWND hDlg, vector<FM*> data){
	long long sum = 0;
	for (int i = 0; i < data.size(); i++){
		sum += _wtoi(data[i]->cash);
	}

	return sum;
}

long double getCategorySum(WCHAR *t, vector<FM*> data){
	long double sum = 0;
	for (int i = 0; i < data.size(); i++){
		if (wcsncmp(data[i]->category, t, wcslen(t)) == 0){
			sum += _wtof(data[i]->cash);
		}
	}
	return sum;
}

float calPercent(long double mem, long double sum){
	return ((mem / sum) * 100);
}

void showDetails(HWND hDlg, vector<FM*> &data){

	WCHAR dt[10001];
	dt[0] = '\0';
	for (int i = 0; i < data.size(); i++){
		wcscat(dt, data[i]->category);
		wcscat(dt, L" - ");
		wcscat(dt, data[i]->details);
		wcscat(dt, L" - ");
		wcscat(dt, data[i]->cash);
		wcscat(dt, L"\r\n");
	}
	SetDlgItemText(hDlg, IDC_EDIT_LIST, dt);

	WCHAR sum[100];
	long long s = showSpentMoney(hDlg, data);
	swprintf(sum, L"%d VND", s);
	SetDlgItemText(hDlg, IDC_EDIT_SUM, sum);
}

void drawPieChart(vector<FM*> data, Graphics *graphics, HWND hDlg){
	WCHAR per[20];
	int linewidth = 3;
	float cor[6];
	HatchBrush *h = new HatchBrush(HatchStyle90Percent, Color(255, 255, 255, 255), Color(255, 255, 255, 255));
	Pen *pen = new Pen(Color(255, 255, 255, 255), linewidth);

	//tô lại biểu đồ tròn sau mỗi lần update để tránh trường hợp lem màu
	graphics->DrawEllipse(pen, 290, 310, 250, 250);
	graphics->FillEllipse(h, 290, 310, 250, 250);

	HatchBrush *hb = new HatchBrush(HatchStyle90Percent, Color(90, 250, 190, 120), Color(90, 250, 190, 120));//light orange
	HatchBrush *hb1 = new HatchBrush(HatchStyle90Percent, Color(255, 0, 255, 255), Color(255, 0, 255, 255));//sky blue
	HatchBrush *hb2 = new HatchBrush(HatchStyle90Percent, Color(255, 0, 255, 0), Color(255, 0, 255, 0));//green
	HatchBrush *hb3 = new HatchBrush(HatchStyle90Percent, Color(255, 255, 0, 0), Color(255, 255, 0, 0));//red
	HatchBrush *hb4 = new HatchBrush(HatchStyle90Percent, Color(255, 255, 255, 0), Color(255, 255, 255, 0));//yellow
	HatchBrush *hb5 = new HatchBrush(HatchStyle90Percent, Color(255, 200, 100, 250), Color(255, 200, 100, 250));//purple

	pen->SetColor(Color(90, 250, 190, 120));//light orange
	float percent = calPercent(getCategorySum(L"Ăn uống", data), showSpentMoney(hDlg, data));
	cor[0] = percent*3.6;
	
	swprintf(per, L"%.2f ", percent);
	wcscat(per, L"%");
	SetWindowText(eating, per);
	SetWindowPos(eating, 0, 90, 410, 55, 15, 0);

	graphics->DrawPie(pen, 290, 310, 250, 250, 0, cor[0]);
	graphics->FillPie(hb, 290, 310, 250, 250, 0, cor[0]);
	graphics->DrawRectangle(pen, 30, 405, 50, 20);
	graphics->FillRectangle(hb, 30, 405, 50, 20);
	
	
	pen->SetColor(Color(255, 0, 255, 255)); //sky blue
	float percent1 = calPercent(getCategorySum(L"Xe cộ", data), showSpentMoney(hDlg, data));
	cor[1] = percent1*3.6;
	
	swprintf(per, L"%.2f ", percent1);
	wcscat(per, L"%");
	SetWindowText(car, per);
	SetWindowPos(car, 0, 215, 410, 55, 15, 0);
	SetDlgItemTextW(hDlg, IDC_CARR, L"Xe cộ");

	graphics->DrawPie(pen, 290, 310, 250, 250, cor[0], cor[1]);
	graphics->FillPie(hb1, 290, 310, 250, 250, cor[0], cor[1]);
	graphics->DrawRectangle(pen, 160, 405, 50, 20);
	graphics->FillRectangle(hb1, 160, 405, 50, 20);
	

	pen->SetColor(Color(255, 0, 255, 0)); //green
	float percent2 = calPercent(getCategorySum(L"Nhu yếu phẩm", data), showSpentMoney(hDlg, data));
	cor[2] = percent2*3.6;

	swprintf(per, L"%.2f ", percent2);
	wcscat(per, L"%");
	SetWindowText(food, per);
	SetWindowPos(food, 0, 90, 465, 55, 15, 0);

	graphics->DrawPie(pen, 290, 310, 250, 250, cor[1] + cor[0], cor[2]);
	graphics->FillPie(hb2, 290, 310, 250, 250, cor[1] + cor[0], cor[2]);
	graphics->DrawRectangle(pen, 30, 460, 50, 20);
	graphics->FillRectangle(hb2, 30, 460, 50, 20);


	pen->SetColor(Color(255, 255, 0, 0)); //red
	float percent3 = calPercent(getCategorySum(L"Nhà cửa", data), showSpentMoney(hDlg, data));
	cor[3] = percent3*3.6;

	swprintf(per, L"%.2f ", percent3);
	wcscat(per, L"%");
	SetWindowText(house, per);
	SetWindowPos(house, 0, 90, 520, 55, 15, 0);

	graphics->DrawPie(pen, 290, 310, 250, 250, cor[2] + cor[1] + cor[0], cor[3]);
	graphics->FillPie(hb3, 290, 310, 250, 250, cor[2] + cor[1] + cor[0], cor[3]);
	graphics->DrawRectangle(pen, 30, 515, 50, 20);
	graphics->FillRectangle(hb3, 30, 515, 50, 20);


	pen->SetColor(Color(255, 255, 255, 0)); //yellow
	float percent4 = calPercent(getCategorySum(L"Di chuyển", data), showSpentMoney(hDlg, data));
	cor[4] = percent4*3.6;

	swprintf(per, L"%.2f ", percent4);
	wcscat(per, L"%");
	SetWindowText(moving, per);
	SetWindowPos(moving, 0, 215, 465, 55, 15, 0);
	
	graphics->DrawPie(pen, 290, 310, 250, 250, cor[3] + cor[2] + cor[1] + cor[0], cor[4]);
	graphics->FillPie(hb4, 290, 310, 250, 250, cor[3] + cor[2] + cor[1] + cor[0], cor[4]);
	graphics->DrawRectangle(pen, 160, 460, 50, 20);
	graphics->FillRectangle(hb4, 160, 460, 50, 20);


	pen->SetColor(Color(255, 200, 100, 250)); //purple
	float percent5 = 100 - (percent + percent1 + percent2 + percent3 + percent4);
	cor[5] = percent5*3.6;

	swprintf(per, L"%.2f ", percent5);
	wcscat(per, L"%");
	SetWindowText(service, per);
	SetWindowPos(service, 0, 215, 520, 55, 15, 0);
	
	graphics->DrawPie(pen, 290, 310, 250, 250, cor[4] + cor[3] + cor[2] + cor[1] + cor[0], cor[5]);
	graphics->FillPie(hb5, 290, 310, 250, 250, cor[4] + cor[3] + cor[2] + cor[1] + cor[0], cor[5]);
	graphics->DrawRectangle(pen, 160, 515, 50, 20);
	graphics->FillRectangle(hb5, 160, 515, 50, 20);

	InvalidateRect(hDlg, NULL, FALSE);

	delete h;
	delete hb;
	delete hb1;
	delete hb2;
	delete hb3;
	delete hb4;
	delete hb5;
	delete pen;
}

void updatePieChart(HWND hDlg, vector<FM*> data){
	PAINTSTRUCT ps;
	HDC hdc;

	hdc = BeginPaint(hDlg, &ps);
	if (graphics != NULL)
		delete graphics;
	graphics = new Graphics(hdc);

	drawPieChart(data, graphics, hDlg);

	InvalidateRect(hDlg, NULL, FALSE);

	delete graphics;
	graphics = NULL;
	EndPaint(hDlg, &ps);
}

bool illegal(WCHAR money[]){
	for (int i = 0; i < wcslen(money);i ++)
	if (money[i] < 48 || money[i] > 57)
		return true;

	return false;
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd = NULL;

   hInst = hInstance; // Store instance handle in our global variable
   paint  = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW | WS_CHILD | WS_VISIBLE,
	   CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   hWnd = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG1), paint, (DLGPROC)FinanceManagement);
   cbb = CreateWindow(L"combobox", L"", WS_CHILD | WS_VISIBLE | CBS_DROPDOWN,
	   34, 78, 100, 300,
	   hWnd, (HMENU)IDC_COMBO1, hInst, NULL);

   eating = CreateWindow(L"static", L"", WS_CHILD | WS_VISIBLE | LWS_TRANSPARENT, 0, 0, 50, 18, hWnd, NULL, hInst, NULL);
   car = CreateWindow(L"static", L"", WS_CHILD | WS_VISIBLE | LWS_TRANSPARENT, 0, 0, 50, 18, hWnd, NULL, hInst, NULL);
   food = CreateWindow(L"static", L"", WS_CHILD | WS_VISIBLE | LWS_TRANSPARENT, 0, 0, 50, 18, hWnd, NULL, hInst, NULL);
   moving = CreateWindow(L"static", L"", WS_CHILD | WS_VISIBLE | LWS_TRANSPARENT, 0, 0, 50, 18, hWnd, NULL, hInst, NULL);
   house = CreateWindow(L"static", L"", WS_CHILD | WS_VISIBLE | LWS_TRANSPARENT, 0, 0, 50, 18, hWnd, NULL, hInst, NULL);
   service = CreateWindow(L"static", L"", WS_CHILD | WS_VISIBLE | LWS_TRANSPARENT, 0, 0, 50, 18, hWnd, NULL, hInst, NULL);
   if (!hWnd)
   {
      return FALSE;
   }
   SetWindowText(hWnd, L"Quản lý chi tiêu");
   SetDlgItemText(hWnd, IDC_ADDTITLE, L"Thêm một loại chi tiêu");
   SetDlgItemText(hWnd, IDC_CATEGORY, L"Loại chi tiêu");
   SetDlgItemText(hWnd, IDC_CONTENT, L"Nội dung");
   SetDlgItemText(hWnd, IDC_MONEY, L"Số tiền");
   SetDlgItemText(hWnd, IDC_ADD, L"Thêm");
   SetDlgItemText(hWnd, IDC_LIST, L"Toàn bộ danh sách các chi tiêu");
   SetDlgItemText(hWnd, IDC_STATISTIC_INFO, L"Thông tin thống kê:");
   SetDlgItemText(hWnd, IDC_SUM, L"Tổng cộng");
   SetDlgItemText(hWnd, IDC_EXIT, L"Thoát");
   
   WCHAR data1[100] = L"Ăn uống";
   WCHAR data2[100] = L"Di chuyển";
   WCHAR data3[100] = L"Nhà cửa";
   WCHAR data4[100] = L"Xe cộ";
   WCHAR data5[100] = L"Nhu yếu phẩm";
   WCHAR data6[100] = L"Dịch vụ";
   ComboBox_AddItemData(cbb, (LPARAM)data1);
   ComboBox_AddItemData(cbb, (LPARAM)data2);
   ComboBox_AddItemData(cbb, (LPARAM)data3);
   ComboBox_AddItemData(cbb, (LPARAM)data4);
   ComboBox_AddItemData(cbb, (LPARAM)data5);
   ComboBox_AddItemData(cbb, (LPARAM)data6);

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LOGFONT lf;
HFONT hFont;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_CREATE:{
					   hFont = CreateFont(lf.lfHeight, lf.lfWidth,
						   lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
						   lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
						   lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
						   lf.lfPitchAndFamily, lf.lfFaceName);

					   INITCOMMONCONTROLSEX icc;
					   icc.dwSize = sizeof(icc);
					   icc.dwICC = ICC_WIN95_CLASSES;
					   InitCommonControlsEx(&icc);

					   GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
					   CreateFont(lf.lfHeight, lf.lfWidth,
						   lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
						   lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
						   lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
						   lf.lfPitchAndFamily, lf.lfFaceName);

					   
					   break;
	}
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:{
					  hdc = BeginPaint(hWnd, &ps);
					  
					  EndPaint(hWnd, &ps);
					  break;
	}
	case WM_DESTROY:
		GdiplusShutdown(gdiplusToken);
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

BOOL CALLBACK FinanceManagement(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam){
	//UNREFERENCED_PARAMETER(lParam);
	HWND hwndOwner;
	RECT rc, rcDlg, rcOwner;

	switch (message)
	{
	case WM_INITDIALOG:{
						   fmdata = readData();
						   showDetails(hDlg, fmdata);
						   return TRUE;
	}
	case WM_COMMAND:{
						int id = LOWORD(wParam);
						switch (id){
						case IDC_ADD:{
										 WCHAR category[30];
										 WCHAR details[1000];
										 WCHAR cash[20];
										 FM *d = NULL;
										 category[0] = '\0';
										 details[0] = '\0';
										 cash[0] = '\0';

										 ComboBox_GetText(cbb, category, 29);
										 GetDlgItemText(hDlg, IDC_EDIT_CONTENT, details, 999);
										 GetDlgItemText(hDlg, IDC_EDIT_MONEY, cash, 19);

										 if (category[0] == '\0' || details[0] == '\0' || cash[0] == '\0'){
											 MessageBox(hDlg, L"Hãy nhập đầy đủ thông tin chi tiêu.", L"Thiếu thông tin", 0);
											 break;
										 }
										 if (illegal(cash)){
											 MessageBox(hDlg, L"Hãy nhập số tiền là số.", L"Số tiền không hợp lệ", 0);
											 break;
										 }

										 d = new FM();
										 wcscpy(d->category, category);
										 wcscpy(d->details, details);
										 wcscpy(d->cash, cash);

										 fmdata.push_back(d);
										 MessageBox(NULL, L"Thêm thành công", L"", MB_OK);
										 SetDlgItemText(hDlg, IDC_COMBO1, L"");
										 SetDlgItemText(hDlg, IDC_EDIT_CONTENT, L"");
										 SetDlgItemText(hDlg, IDC_EDIT_MONEY, L"");

										 showDetails(hDlg, fmdata);
										 
										 updatePieChart(hDlg, fmdata);
										 break;
						}
						case IDC_EXIT:
							writeData(fmdata);
							EndDialog(hDlg, TRUE);
							DestroyWindow(hDlg);
							PostQuitMessage(0);
							return TRUE;
							break;
						case IDCANCEL:
							writeData(fmdata);
							EndDialog(hDlg, TRUE);
							DestroyWindow(hDlg);
							PostQuitMessage(0);
							return TRUE;
							break;
						}
						break;
	}
	case WM_PAINT:{
					  PAINTSTRUCT ps;
					  HDC hdc;
					  hdc = BeginPaint(hDlg, &ps);

					  graphics = new Graphics(hdc);
					  
					  
					  drawPieChart(fmdata, graphics, hDlg);
					  
					  delete graphics;
					  //EndPaint(hWnd, &ps);
					  graphics = NULL;
					  EndPaint(hDlg, &ps);
					  break;
	}
	}
	return FALSE;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}