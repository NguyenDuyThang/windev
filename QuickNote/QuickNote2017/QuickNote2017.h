﻿#pragma once

#include "resource.h"
#include <vector>
#include <iostream>

using namespace std;

#define NULLKEY (WCHAR*)""
#define MAX_SIZE_HASHTABLE 1000

struct NODENOTE{
	WCHAR tag[1000];
	WCHAR note[1000];
	NODENOTE *next;
};

bool isEmpty();
bool isFull();
void initialize();
int hashTag(WCHAR *tag);
int searchTag(WCHAR *tag);
int insertTagAndNote(WCHAR *tag);

extern NODENOTE **listnote;
extern int n;
