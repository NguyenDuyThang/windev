//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FinanceManagemant.rc
//
#define IDC_MYICON                      2
#define IDD_FINANCEMANAGEMANT_DIALOG    102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_FINANCEMANAGEMANT           107
#define IDI_SMALL                       108
#define IDC_FINANCEMANAGEMANT           109
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDI_ICON1                       132
#define IDC_CATEGORY                    1002
#define IDC_CONTENT                     1003
#define IDC_ADD                         1004
#define IDC_MONEY                       1005
#define IDC_EDIT_CONTENT                1007
#define IDC_EDIT_MONEY                  1008
#define IDC_ADDTITLE                    1009
#define IDC_LIST                        1010
#define IDC_EDIT_LIST                   1011
#define IDC_STATISTIC_INFO              1012
#define IDC_SUM                         1013
#define IDC_EDIT_SUM                    1014
#define IDC_EXIT                        1015
#define IDC_EATING                      1017
#define IDC_COMBO1                      1018
#define IDC_CAR                         1018
#define IDC_CARR                        1018
#define IDC_FOOD                        1019
#define IDC_MOVING                      1020
#define IDC_HOUSE                       1021
#define IDC_SERVICE                     1022
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1023
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
