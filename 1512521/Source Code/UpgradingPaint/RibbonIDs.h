// *****************************************************************************
// * This is an automatically generated header file for UI Element definition  *
// * resource symbols and values. Please do not modify manually.               *
// *****************************************************************************

#pragma once

#define ID_SHAPES_LINE 2 
#define ID_SHAPES_LINE_LabelTitle_RESID 60001
#define ID_SHAPES_LINE_LargeImages_RESID 201
#define ID_SHAPES_RECTANGLE 3 
#define ID_SHAPES_RECTANGLE_LabelTitle_RESID 60002
#define ID_SHAPES_RECTANGLE_LargeImages_RESID 60003
#define ID_SHAPES_ELLIPSE 4 
#define ID_SHAPES_ELLIPSE_LabelTitle_RESID 60004
#define ID_SHAPES_ELLIPSE_LargeImages_RESID 60005
#define ID_SHAPES_SQUARE 5 
#define ID_SHAPES_SQUARE_LabelTitle_RESID 60006
#define ID_SHAPES_SQUARE_LargeImages_RESID 60007
#define ID_SHAPES_CIRCLE 6 
#define ID_SHAPES_CIRCLE_LabelTitle_RESID 60008
#define ID_SHAPES_CIRCLE_LargeImages_RESID 60009
#define G1_LabelTitle_RESID 60010
#define G2_LabelTitle_RESID 60011
#define G3_LabelTitle_RESID 60012
#define G4_LabelTitle_RESID 60013
#define G5_LabelTitle_RESID 60014
#define InternalCmd2_LabelTitle_RESID 60015
#define InternalCmd4_LabelTitle_RESID 60016
