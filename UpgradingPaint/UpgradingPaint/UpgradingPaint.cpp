﻿// UpgradingPaint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "UpgradingPaint.h"
#include "RibbonFramework.h"
#include "RibbonIDs.h"
#include "Shape.h"
#include "PaintLib.h"
#include <vector>
#include <windowsx.h>
#include <Objbase.h>
#pragma comment(lib, "Ole32.lib")

using namespace std;

#define MAX_LOADSTRING 100

typedef int(__cdecl *GETCOR)(int, int, int);
typedef double(__cdecl *MYPROC)(double, double);

HINSTANCE hinstLib;
MYPROC ProcGet = NULL;
GETCOR ProcGetCor = NULL;

SHape *s = NULL;
Pen *pen = NULL;
Point start;
Point enD;
Point startConst;
int lineWidth = 3;
bool isDrawing = false;
bool isPressingShift = false;
bool inSquareMode = false;
bool inCircleMode = false;
int shapeToDraw = 0; //0 :chưa xác định hình vẽ
//1 : line
//2 : rectangle (hoặc square)
//3 : ellipse

GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR gdiplusToken;

vector<SHape*> shape;

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	HRESULT hr = CoInitialize(NULL);
	if (FAILED(hr)){
		return FALSE;
	}

	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_UPGRADINGPAINT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_UPGRADINGPAINT));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	CoUninitialize();

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = 0; //CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_UPGRADINGPAINT));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_UPGRADINGPAINT);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	bool initSuccess;

	switch (message)
	{
	case WM_CREATE:
	{
					  //Initializes the Ribbon framework.
					  initSuccess = InitializeFramework(hWnd);
					  GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

					  if (!initSuccess){
						  return -1;
					  }
					  break;
	}
	case WM_MOUSEMOVE:
	{
						 return HANDLE_WM_MOUSEMOVE(hWnd, wParam, lParam, OnMouseMove);
						 break;
	}
	case WM_KEYDOWN: {
						 if ((wParam & MK_SHIFT) == 0) { //0: nhấn
							 isPressingShift = true;
							 if (shapeToDraw == RECTANGLE)
								 inSquareMode = true;
							 else
							 if (shapeToDraw == ELLIPSE)
								 inCircleMode = true;
							//MessageBox(hWnd, L"You are pressing Shift", L"", MB_OK);
						 }
						 break;
	}
	case WM_KEYUP:{
					  isPressingShift = false;

					  if (inSquareMode){
						  inSquareMode = false;
						  shapeToDraw = RECTANGLE;
					  }
					  else
					  if (inCircleMode){
						  inCircleMode = false;
						  shapeToDraw = ELLIPSE;
					  }
					  break;
	}
	case WM_LBUTTONDOWN:
	{
						   return HANDLE_WM_LBUTTONDOWN(hWnd, wParam, lParam, OnLButtonDown);
						   break;
	}
	case WM_LBUTTONUP:
	{
						 return HANDLE_WM_LBUTTONUP(hWnd, wParam, lParam, OnLButtonUp);
						 break;
	}
	case WM_COMMAND:
			return HANDLE_WM_COMMAND(hWnd, wParam, lParam, OnCommand);
		break;
	case WM_PAINT:{
					  return HANDLE_WM_PAINT(hWnd, wParam, lParam, OnPaint);
					  break;
	}
	case WM_DESTROY:
					return HANDLE_WM_DESTROY(hWnd, wParam, lParam, OnDestroy);
					break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void OnLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags){
	if (shapeToDraw != 0){
		start.X = x;
		start.Y = y;
		startConst.X = x;
		startConst.Y = y;
		isDrawing = true;
		SetCapture(hwnd);
	}
	else
		MessageBox(hwnd, L"Hãy chọn hình muốn vẽ!", L"Chưa chọn hình vẽ", MB_OK);
}
void OnLButtonUp(HWND hwnd, int x, int y, UINT keyFlags){
	enD.X = x;
	enD.Y = y;
	isDrawing = false;

	switch (shapeToDraw){
	case LINE:{
				  LIne *line = new LIne();
				  line->setStartPoint(start);
				  line->setEndPoint(enD);
				  shape.push_back(line);
				  break;
	}
	case RECTANGLE:{
					   REctangle *rectangle = new REctangle();
					   
					   rectangle->setStartPoint(start);
					   rectangle->setEndPoint(enD);

					   rectangle->setWidth(abs(enD.X - start.X));
					   rectangle->setHeight(abs(enD.Y - start.Y));

					   shape.push_back(rectangle);
					   break;
	}
	case SQUARE:{
					//DLL
					int width = 0;
					int height = 0;
					hinstLib = LoadLibrary(L"Paint_DLL.dll");
					if (hinstLib){
						ProcGet = (MYPROC)GetProcAddress(hinstLib, "Sizee_getWidth");
						if (ProcGet != NULL){
							width = abs(ProcGet(enD.X, start.X));
						}
						ProcGet = NULL;
						ProcGet = (MYPROC)GetProcAddress(hinstLib, "Sizee_getHeight");
						if (ProcGet != NULL){
							height = abs(ProcGet(enD.Y, start.Y));
						}
					}

					int difference = abs(height - width);

					if (width > height){
						if (enD.X < start.X){
							enD.X += difference;
							width = height = start.X - enD.X;
						}
						else{
							enD.X -= difference;
							width = height = enD.X - start.X;
						}
					}
					else{
						if (enD.Y < start.Y){
							enD.Y += difference;
							width = height = start.Y - enD.Y;
						}
						else{
							enD.Y -= difference;
							width = height = enD.Y - start.Y;
						}
					}

					REctangle *rectangle = new REctangle();
					rectangle->setStartPoint(start);
					rectangle->setEndPoint(enD);
					rectangle->setWidth(width);
					rectangle->setHeight(height);
					shape.push_back(rectangle);
					break;
	}
	case ELLIPSE:{
					 ELlipse *ellipse = new ELlipse();
					 ellipse->setStartPoint(start);
					 ellipse->setWidth(enD.X - start.X);
					 ellipse->setHeight(enD.Y - start.Y);
					 shape.push_back(ellipse);
					 break;
	}
	case CIRCLE:{
					//DLL
					int width = 0;
					int height = 0;
					hinstLib = LoadLibrary(L"Paint_DLL.dll");
					if (hinstLib){
						ProcGet = (MYPROC)GetProcAddress(hinstLib, "Sizee_getWidth");
						if (ProcGet != NULL){
							width = abs(ProcGet(enD.X, start.X));
						}
						ProcGet = NULL;
						ProcGet = (MYPROC)GetProcAddress(hinstLib, "Sizee_getHeight");
						if (ProcGet != NULL){
							height = abs(ProcGet(enD.Y, start.Y));
						}
						ProcGet = NULL;
					}

					int difference = abs(height - width);

					if (width > height){

						ProcGetCor = (GETCOR)GetProcAddress(hinstLib, "getCor");
						if (ProcGetCor != NULL){
							enD.X = ProcGetCor(start.X, enD.X, difference);
						}
						ProcGetCor = NULL;
					}
					else{

						ProcGetCor = (GETCOR)GetProcAddress(hinstLib, "getCor");
						if (ProcGetCor != NULL){
							enD.Y = ProcGetCor(start.Y, enD.Y, difference);
						}
						ProcGetCor = NULL;
					}

					ELlipse *ellipse = new ELlipse();
					ellipse->setStartPoint(start);
					ellipse->setWidth(enD.X - start.X);
					ellipse->setHeight(enD.Y - start.Y);
					shape.push_back(ellipse);
					break;
	}
	}

	InvalidateRect(hwnd, NULL, FALSE);
	ReleaseCapture();
}
void OnMouseMove(HWND hwnd, int x, int y, UINT keyFlags){
	WCHAR pos[200];
	wsprintf(pos, L"%d  %d", x, y);
	SetWindowText(hwnd, pos);

	if (isDrawing){
		enD.X = x;
		enD.Y = y;

		InvalidateRect(hwnd, NULL, FALSE);
	}
}
void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify){
	/*wmId = LOWORD(wParam);
	wmEvent = HIWORD(wParam);*/
	HMENU hMenu = GetMenu(hwnd);

	// Parse the menu selections:
	switch (id)
	{
	case ID_SHAPES_LINE:
		CheckMenuItem(hMenu, ID_SHAPES_LINE, MF_CHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_RECTANGLE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_ELLIPSE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_SQUARE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_CIRCLE, MF_UNCHECKED);
		shapeToDraw = LINE;
		break;
	case ID_SHAPES_RECTANGLE:
		CheckMenuItem(hMenu, ID_SHAPES_LINE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_RECTANGLE, MF_CHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_ELLIPSE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_SQUARE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_CIRCLE, MF_UNCHECKED);
		shapeToDraw = RECTANGLE;
		break;
	case ID_SHAPES_ELLIPSE:
		CheckMenuItem(hMenu, ID_SHAPES_LINE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_RECTANGLE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_ELLIPSE, MF_CHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_SQUARE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_CIRCLE, MF_UNCHECKED);
		shapeToDraw = ELLIPSE;
		break;
	case ID_SHAPES_SQUARE:
		CheckMenuItem(hMenu, ID_SHAPES_LINE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_RECTANGLE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_ELLIPSE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_SQUARE, MF_CHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_CIRCLE, MF_UNCHECKED);
		shapeToDraw = SQUARE;
		break;
	case ID_SHAPES_CIRCLE:
		CheckMenuItem(hMenu, ID_SHAPES_LINE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_RECTANGLE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_ELLIPSE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_SQUARE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SHAPES_CIRCLE, MF_CHECKED);
		shapeToDraw = CIRCLE;
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hwnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hwnd);
		break;
	}
}

void OnPaint(HWND hwnd){
	PAINTSTRUCT ps;
	HDC hdc;
	hdc = BeginPaint(hwnd, &ps);
	HDC Memhdc = NULL;
	RECT rect;
	GetClientRect(hwnd, &rect);
	HBITMAP hbitmap;

	Memhdc = CreateCompatibleDC(hdc);
	hbitmap = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
	SelectObject(Memhdc, hbitmap);
	FillRect(Memhdc, &rect, (HBRUSH)(COLOR_WINDOW + 1));

	if (pen == NULL){
		pen = new Pen(Color(255, 0, 0, 0), lineWidth);
	}

	Graphics *graphics = new Graphics(Memhdc);

	//vẽ các hình theo thứ tự
	for (int i = 0; i < shape.size(); i++)
		shape[i]->draw(graphics);
	
	if (isPressingShift && inSquareMode)
		shapeToDraw = SQUARE;
	else
	if (isPressingShift && inCircleMode)
		shapeToDraw = CIRCLE;

	
	if (isDrawing){
		switch (shapeToDraw){
		case LINE:{
					  s = new LIne();
					  s->preview(graphics, start, enD);
					  delete s;
					  break;
		}
		case RECTANGLE:
			s = new REctangle();
			s->preview(graphics, start, enD);
			delete s;
			break;
		case SQUARE:
		{
					   //DLL
					   int width = 0;
					   int height = 0;
					   hinstLib = LoadLibrary(L"Paint_DLL.dll");
					   if (hinstLib){
						   ProcGet = (MYPROC)GetProcAddress(hinstLib, "Sizee_getWidth");
						   if (ProcGet != NULL){
							   width = abs(ProcGet(enD.X, start.X));
						   }
						   ProcGet = NULL;
						   ProcGet = (MYPROC)GetProcAddress(hinstLib, "Sizee_getHeight");
						   if (ProcGet != NULL){
							   height = abs(ProcGet(enD.Y, start.Y));
						   }
					   }

					   int difference = abs(height - width);

					   if (width > height){
						   if (enD.X < start.X)
							   enD.X += difference;
						   else
							   enD.X -= difference;
					   }
					   else{
						   if (enD.Y < start.Y)
							   enD.Y += difference;
						   else
							   enD.Y -= difference;
					   }
					   s = new REctangle();
					   s->preview(graphics, start, enD);
					   delete s;
					   break;
		}
		case ELLIPSE:
			s = new ELlipse();
			s->preview(graphics, start, enD);
			delete s;
			break;
		case CIRCLE:
			//DLL
			int width = 0;
			int height = 0;
			hinstLib = LoadLibrary(L"Paint_DLL.dll");
			if (hinstLib){
				ProcGet = (MYPROC)GetProcAddress(hinstLib, "Sizee_getWidth");
				if (ProcGet != NULL){
					width = abs(ProcGet(enD.X, start.X));
				}
				ProcGet = NULL;
				ProcGet = (MYPROC)GetProcAddress(hinstLib, "Sizee_getHeight");
				if (ProcGet != NULL){
					height = abs(ProcGet(enD.Y, start.Y));
				}
			}

			int difference = abs(height - width);

			if (width > height){

				ProcGetCor = (GETCOR)GetProcAddress(hinstLib, "getCor");
				if (ProcGetCor != NULL){
					enD.X = ProcGetCor(start.X, enD.X, difference);
				}
				ProcGetCor = NULL;
			}
			else{

				ProcGetCor = (GETCOR)GetProcAddress(hinstLib, "getCor");
				if (ProcGetCor != NULL){
					enD.Y = ProcGetCor(start.Y, enD.Y, difference);
				}
				ProcGetCor = NULL;
			}

			s = new ELlipse();
			s->preview(graphics, start, enD);
			delete s;
			break;
		}
	}
	int RibbonHeight = GetRibbonHeight();
	BitBlt(hdc, 0, RibbonHeight, rect.right, rect.bottom, Memhdc, 0, RibbonHeight, SRCCOPY);
	delete graphics;
	EndPaint(hwnd, &ps);
}
void OnDestroy(HWND hwnd){
	GdiplusShutdown(gdiplusToken);
	shape.clear();
	PostQuitMessage(0);
}
