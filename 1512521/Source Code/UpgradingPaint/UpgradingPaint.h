#pragma once

#include "resource.h"

#define LINE 1
#define RECTANGLE 2
#define ELLIPSE 3
#define SQUARE 4
#define CIRCLE 5

extern int shapeToDraw;

void OnKey(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags);
void OnLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
void OnLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
void OnMouseMove(HWND hwnd, int x, int y, UINT keyFlags);
void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hwnd);
void OnDestroy(HWND hwnd);