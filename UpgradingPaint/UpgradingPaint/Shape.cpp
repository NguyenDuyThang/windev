#include "stdafx.h"
#include "Shape.h"


SHape::SHape()
{
	this->pen = new Pen(Color(255, 0, 0, 0), 3);
	/*this->isDrawing = false;
	this->isPressingShift = false;
	this->inSquareMode = false;
	this->inCircleMode = false;*/
}


SHape::~SHape()
{
	delete this->pen;
}

LIne::LIne(){
	this->setStartPoint(Point(0, 0));
	this->setEndPoint(Point(0, 0));
}

void LIne::draw(Graphics *g){
	g->DrawLine(this->getPen(), this->getStartPoint(), this->getEndPoint());
}

void LIne::preview(Graphics *g, Point start, Point end){
	g->DrawLine(this->getPen(), start, end);
}

LIne::~LIne(){

}

REctangle::REctangle(){
	this->setStartPoint(Point(0, 0));
	this->setWidth(0);
	this->setHeight(0);
}

void REctangle::draw(Graphics *g){
	if (this->getEndPoint().X < this->getStartPoint().X 
		&& this->getEndPoint().Y < this->getStartPoint().Y){
		
		g->DrawRectangle(this->getPen(), this->getEndPoint().X, this->getEndPoint().Y, this->getWidth(), this->getHeight());
	}
	else if (this->getEndPoint().X < this->getStartPoint().X 
		&& this->getEndPoint().Y > this->getStartPoint().Y){

		g->DrawRectangle(this->getPen(), this->getEndPoint().X, this->getStartPoint().Y, this->getWidth(), this->getHeight());
	}
	else if (this->getEndPoint().X > this->getStartPoint().X 
		&& this->getEndPoint().Y < this->getStartPoint().Y){

		g->DrawRectangle(this->getPen(), this->getStartPoint().X, this->getEndPoint().Y, this->getWidth(), this->getHeight());
	}
	else
		g->DrawRectangle(this->getPen(), this->getStartPoint().X, this->getStartPoint().Y, this->getWidth(), this->getHeight());
}

void REctangle::preview(Graphics *g, Point start, Point end){
	if (end.X < start.X && end.Y < start.Y){
		g->DrawRectangle(this->getPen(), end.X, end.Y, start.X - end.X, start.Y - end.Y);
	}
	else if (end.X < start.X && end.Y > start.Y){
		g->DrawRectangle(this->getPen(), end.X, start.Y, start.X - end.X, end.Y - start.Y);
	}
	else if (end.X > start.X && end.Y < start.Y){
		g->DrawRectangle(this->getPen(), start.X, end.Y, end.X - start.X, start.Y - end.Y);
	}
	else{
		g->DrawRectangle(this->getPen(), start.X, start.Y, end.X - start.X, end.Y - start.Y);
	}
}

REctangle::~REctangle(){

}

ELlipse::ELlipse(){
	this->setStartPoint(Point(0, 0));
	this->setWidth(0);
	this->setHeight(0);
}

void ELlipse::draw(Graphics *g){
	g->DrawEllipse(this->getPen(), this->getStartPoint().X, this->getStartPoint().Y, this->getWidth(), this->getHeight());
}

void ELlipse::preview(Graphics *g, Point start, Point end){
	g->DrawEllipse(this->getPen(), start.X, start.Y, end.X - start.X, end.Y - start.Y);
}

ELlipse::~ELlipse(){

}