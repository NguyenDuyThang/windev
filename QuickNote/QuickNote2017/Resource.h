//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by QuickNote2017.rc
//
#define IDC_MYICON                      2
#define IDD_QUICKNOTE2017_DIALOG        102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_QUICKNOTE2017               107
#define IDI_SMALL                       108
#define IDC_QUICKNOTE2017               109
#define ID_VIEWNOTE                     110
#define ID_VIEWSTATISTICS               111
#define ID_EXIT                         112
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDD_DIALOG2                     132
#define IDD_TAGCLOUD                    132
#define IDD_BROWSENOTE                  133
#define IDD_BROWSETAG                   134
#define IDC_NOTE                        1001
#define IDC_TAG                         1002
#define IDC_NOTESTATIC                  1003
#define IDC_TAGSTATIC                   1004
#define IDC_ADD                         1005
#define IDC_CANCEL                      1006
#define IDC_TAGCLOUD                    1007
#define IDC_BROWSENOTE                  1008
#define IDC_EDITNOTE                    1008
#define IDC_BROWSETAG                   1009
#define IDC_STATICNOTE                  1009
#define IDC_RESULT                      1010
#define IDC_STATICKQ                    1011
#define IDC_STATICTAGS                  1013
#define IDFIND                          1014
#define IDC_STATICTAG                   1015
#define IDC_EDITTAGS                    1016
#define IDFINDTAG                       1017
#define IDCANCELTAG                     1018
#define IDC_ADDNOTE						1019
#define IDC_DELETENOTE					1020
#define IDC_VIEWALLNOTES				1021
#define IDC_REFRESH						1022
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           113
#endif
#endif
