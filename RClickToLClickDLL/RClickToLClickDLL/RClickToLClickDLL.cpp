﻿// RClickToLClickDLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "RToLDLL.h"

HHOOK hHook = NULL;
HINSTANCE hinstLib;
bool settingHookMouse = false;

LRESULT CALLBACK MouseHookProc(int nCode, WPARAM wParam, LPARAM lParam){
	if (nCode < 0)
		return CallNextHookEx(hHook, nCode, wParam, lParam);
	MOUSEHOOKSTRUCT *mhs = (PMOUSEHOOKSTRUCT )lParam;
	LPMSG msg = (LPMSG)lParam;
	
	SwapMouseButton(true);

	return CallNextHookEx(hHook, nCode, wParam, lParam);
}

void installHook(HWND hwnd){
	if (hHook == NULL){
		hHook = SetWindowsHookEx(WH_MOUSE_LL, (HOOKPROC)MouseHookProc, hinstLib, 0);
		if (hHook){
			MessageBox(hwnd, L"Set up hook Succeesfully", 0, 0);
		}
		else
			MessageBox(hwnd, L"Setup hook fail", L"Result", MB_OK);
	}
}

bool uninstallHook(HWND hwnd)
{
	if (hHook != NULL){
		UnhookWindowsHookEx(hHook);
		hHook = NULL;
		MessageBox(hwnd, L"Remove hook successfully", L"Result", MB_OK);
		SwapMouseButton(false);
		return true;
	}
	return false;
}
