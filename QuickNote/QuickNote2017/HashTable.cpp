﻿#include "stdafx.h"
#include "QuickNote2017.h"


bool isEmpty(){
	if (n == 0)
		return true;
	return false;
}

bool isFull(){
	if (n == 1000)
		return true;
	return false;
}

void initialize(){
	listnote = new NODENOTE*[MAX_SIZE_HASHTABLE];

	for (int i = 0; i < MAX_SIZE_HASHTABLE; i++){

		listnote[i] = new NODENOTE();
		memset(listnote[i]->note, 0, 1000);
		memset(listnote[i]->tag, 0, 1000);
		listnote[i]->next = NULL;
	}

	n = 0;
}

int hashTag(WCHAR *tag){
	int index = 0;

	for (int i = 0; i < wcslen(tag); i++){
		index += tag[i];
	}

	while (index > 1000){
		index /= 10;
	}

	return index;
}

int searchTag(WCHAR *tag){
	if (!isEmpty()){
		int i = hashTag(tag);
		while (wcscmp(listnote[i]->tag, tag) != 0 && listnote[i]->tag != NULLKEY){
			i++;
			if (i >= 1000)
				i = i - 1000;
		}
		if (wcscmp(listnote[i]->tag, tag) == 0)
			return i;

		return -1; //NULLKEY
	}
	else{
		MessageBox(0, L"Hash Table rỗng", L"Dữ liệu rỗng", 0);
		return -1; //NULLKEY
	}
}

int insertTagAndNote(WCHAR *tag){
	if (!isFull()){
		int i = hashTag(tag);

		while (wcscmp(listnote[i]->tag, NULLKEY) != 0){

			if ((wcscmp(listnote[i]->tag, tag) == 0)){//nếu trùng
				return i;
			}

			i++;
			if (i >= 1000)
				i = i - 1000;
		}
		wcscpy(listnote[i]->tag, tag);
		n++;
		
		return i;
	}
	else{
		return -1; //NULLKEY
	}
}