#pragma once
#include <objidl.h>
#include <gdiplus.h>
#pragma comment(lib, "gdiplus.lib")

using namespace Gdiplus;

class SHape
{
private:
	Pen *pen;
public:
	void setPen(Pen *pen){ this->pen = pen; }
	/*void setDrawingStatus(bool isDrawing){ this->isDrawing = isDrawing; }
	void setShiftStatus(bool isPressingShift){ this->isPressingShift = isPressingShift; }
	void setSquareMode(bool inSquareMode){ this->inSquareMode = inSquareMode; }
	void setCircleMode(bool inCircleMode){ this->inCircleMode = inCircleMode; }*/

	Pen* getPen(){ return this->pen; }
	/*bool getDrawingStatus(){ return this->isDrawing; }
	bool getShiftStatus(){ return this->isPressingShift; }
	bool getSquareMode(){ return this->inSquareMode; }
	bool getCircleMode(){ return this->inCircleMode; }*/
	SHape();
	virtual void draw(Graphics *g) = 0;
	virtual void preview(Graphics *g, Point start, Point end) = 0;
	~SHape();
};

class LIne: public SHape{
private:
	Point start;
	Point end;
public:
	void setStartPoint(Point p){ this->start.X = p.X; this->start.Y = p.Y; }
	void setEndPoint(Point p){ this->end.X = p.X; this->end.Y = p.Y; }
	Point getStartPoint(){ return this->start; }
	Point getEndPoint(){ return this->end; }
	LIne();
	void draw(Graphics *g);
	void preview(Graphics *g, Point start, Point end);
	~LIne();
};

class REctangle: public SHape{
private:
	Point start;
	Point end;
	int width;
	int height;
public:
	void setStartPoint(Point p){ this->start.X = p.X; this->start.Y = p.Y; }
	void setEndPoint(Point p){ this->end.X = p.X; this->end.Y = p.Y; }
	void setWidth(int width){ this->width = width; }
	void setHeight(int height){ this->height = height; }

	Point getStartPoint(){ return this->start; }
	Point getEndPoint(){ return this->end; }
	int getWidth(){ return this->width; }
	int getHeight(){ return this->height; }
	REctangle();
	void draw(Graphics *g);
	void preview(Graphics *g, Point start, Point end);
	~REctangle();
};

class ELlipse : public SHape{
private:
	Point start;
	Point end;
	int width;
	int height;
public:
	void setStartPoint(Point p){ this->start.X = p.X; this->start.Y = p.Y; }
	void setEndPoint(Point p){ this->end.X = p.X; this->end.Y = p.Y; }
	void setWidth(int width){ this->width = width; }
	void setHeight(int height){ this->height = height; }

	Point getStartPoint(){ return this->start; }
	Point getEndPoint(){ return this->end; }
	int getWidth(){ return this->width; }
	int getHeight(){ return this->height; }
	ELlipse();
	void draw(Graphics *g);
	void preview(Graphics *g, Point start, Point end);
	~ELlipse();
};

