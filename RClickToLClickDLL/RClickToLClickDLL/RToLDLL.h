#pragma once
#include "stdafx.h"
#include <Windowsx.h>
#include <Windows.h>

#ifdef RTOLLIBRARY_EXPORTS
#define RTOLLIBRARY_EXPORTS __declspec(dllexport)
#else
#define RTOLLIBRARY_API __declspec(dllimport)
#endif

LRESULT CALLBACK MouseHookProc(int nCode, WPARAM wParam, LPARAM lParam);
extern "C" RTOLLIBRARY_API void installHook(HWND hWnd);
extern "C" RTOLLIBRARY_API bool uninstallHook(HWND hWnd);