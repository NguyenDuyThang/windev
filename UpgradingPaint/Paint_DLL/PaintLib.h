#pragma once

#ifdef PAINTLIBRARY_EXPORTS
#define PAINTLIBRARY_API __declspec(dllexport) 
#else
#define PAINTLIBRARY_API __declspec(dllimport) 
#endif

namespace PaintLibrary{
	class Sizee{
	public:
		static PAINTLIBRARY_API double getWidth(double width, double height);
		static PAINTLIBRARY_API double getHeight(double width, double height);
	};
}

extern "C" PAINTLIBRARY_API int getCor(int start, int end, int difference);