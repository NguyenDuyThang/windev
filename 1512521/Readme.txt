Nguyễn Duy Thắng
1512521

Những chức năng đã làm được:
- Sử dụng Shell để lấy các đối trượng thư mục và tập tin của các ổ đĩa.
- Thay đổi kích thước treeview thì listview thay đổi theo.
- Thanh Statusbar ở dưới cùng cửa sổ hiển thị kích thước tập tin tương ứng khi double click vào tập tin đó.
- Lưu kích thước cửa sổ màn hình chính và nạp lại khi chương trình chạy lên.

