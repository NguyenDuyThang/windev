﻿// QuickNote2017.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "QuickNote2017.h"
#include <windowsX.h>
#include <winuser.h>
#include <CommCtrl.h>
#include <string>
#include <locale>
#include <codecvt> //possible C++11?
#include <fcntl.h> //_O_WTEXT
#include <io.h>    //_setmode()
#include <fstream>
#include <objidl.h>
#include <gdiplus.h>
#include <time.h>
#include <conio.h>
#pragma comment(lib, "gdiplus.lib")

using namespace Gdiplus;

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")

#define MAX_LOADSTRING 100


// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
HWND viewTag = NULL;
HWND viewContent = NULL;
HWND viewTagBrowsed = NULL;
HWND viewContentBrowsed = NULL;
HWND editNote = NULL;
HWND editTag = NULL;
HWND hCloudDlg = NULL;

vector<LVITEM*> lvi;
vector<LVITEM*> lviContent;
vector<LVITEM*> lviBrowsed;
vector<LVITEM*> lviContentBrowsed;
vector<HWND*> tc;// danh sách handle tagcloud
vector<WCHAR*> cnt;//content

int n;//số phần tử hiện có trong listnote
WCHAR tagContent[1000];//lưu chuỗi tag đang được hiển thị nội dung
NODENOTE **listnote = NULL; //danh sách các note kèm theo tag, có 1000 note trong list
Graphics *graphics = NULL;
GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR gdiplusToken;
LOGFONT lf;
HFONT hFont;
HMENU hPopupMenu;
POINT point;
HCOLORSPACE hColor;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	QuickNote(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK	TagCloud(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK	browseNote(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK	browseTag(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	_setmode(_fileno(stdout), _O_WTEXT); //needed for output

	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_QUICKNOTE2017, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_QUICKNOTE2017));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_QUICKNOTE2017));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_QUICKNOTE2017);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
NODENOTE** initializeNODENOTE(){
	NODENOTE **data = new NODENOTE*[MAX_SIZE_HASHTABLE];
	for (int i = 0; i < MAX_SIZE_HASHTABLE; i++){
		data[i] = new NODENOTE();
		memset(data[i]->note, 0, 1000);
		memset(data[i]->tag, 0, 1000);
		data[i]->next = NULL;
	}
	return data;
}

void writeData(){
	_setmode(_fileno(stdout), _O_WTEXT); //needed for output
	std::locale loc(std::locale(), new std::codecvt_utf8<wchar_t>);  // UTF-8
	std::wofstream fout(L"data.txt");
	if (!fout) {
		std::wcout << L"Không thể tạo file data.txt\n";
	}
	else {
		fout.imbue(loc);
		int count = 0;
		for (int i = 0; i < MAX_SIZE_HASHTABLE; i++){
			if (wcscmp(listnote[i]->tag, NULLKEY) != 0){
				fout << listnote[i]->tag << L",-.";
				NODENOTE *temp = listnote[i]->next;
				while (temp != NULL){
						fout << temp->note << L",-.";
					temp = temp->next;
				}
				if (count != lvi.size() - 1)
					fout << L'\n';
				count++;
			}
		}
	}
	fout.close();
}

NODENOTE** readData(){
	_setmode(_fileno(stdin), _O_WTEXT); //needed for input
	locale loc(std::locale(), new codecvt_utf8<wchar_t>);  // UTF-8

	wifstream fin(L"data.txt");

	NODENOTE **data = initializeNODENOTE();

	if (!fin) {
		wcout << L"Không thể đọc file data.txt\n";
	}
	else {
		WCHAR t[100000];
		WCHAR tag[1000];
		WCHAR note[1000];
		WCHAR temp[1000];

		while (!fin.eof()){
			memset(tag, 0, 1000);
			memset(note, 0, 1000);
			memset(t, 0, 100000);
			fin.imbue(loc);

			string s;
			while (1){
				fin >> temp;
				
				wcscat(temp, L" ");//do đọc dữ liệu vào bị mất dấu cách nên thêm lại
				wcscat(t, temp);

				//if (fin.eof())//hết file
				//	return data;
				if (temp == L" ")
					break;
				if(temp[wcslen(temp) - 4] == L',' && temp[wcslen(temp) - 3] == L'-' && temp[wcslen(temp) - 2] == L'.'){//,-. đứng cuối dòng thì tách ra là 1 tag và các note liên quan
					t[wcslen(t) - 4] = '\0'; //xóa dấu hiệu kết thúc 1 tag
					break;
				}

			}

			n++;//tăng số phần tử trong hashtable

			//tách tag để băm vào hash table, sau đó tách từng note
			if (t[0] == ',' && t[1] == '-' && t[2] == '.')//rỗng
				break;
			int i = 0;
			for (int j = 0; j < wcslen(t); j++){//tách tag
				if (t[j] != ',' || t[j + 1] != '-' || t[j + 2] != '.')
					tag[j] = t[j];
				else{
					i = j + 3;
					break;
				}
			}

			int index = hashTag(tag);
			wcscpy(data[index]->tag, tag);
			int k = 0;
			for (int j = i; j < wcslen(t); j++){//bắt đầu tiếp tục sau tag
				if (t[j] != ',' || t[j + 1] != '-' || t[j + 2] != '.'){
					note[k] = t[j];
					k++;

					if (j == wcslen(t) - 1){
						NODENOTE *temp = data[index];
						while (temp->next != NULL)
							temp = temp->next;

						temp->next = new NODENOTE();
						wcscpy(temp->next->note, note);
					}
				}
				else{
					NODENOTE *temp = data[index];
					while (temp->next != NULL)
						temp = temp->next;

					temp->next = new NODENOTE();
					wcscpy(temp->next->note, note);
					memset(note, 0, 1000);
					k = 0;
					j += 2;
				}
			}
		}
		fin.close();
	}
	return data;
}

vector<WCHAR*> seperatingTags(WCHAR tag[]){
	vector<WCHAR*> tempTags;
	WCHAR *temp = new WCHAR[100];
	int k = 0;
	memset(temp, 0, 100);
	int len = wcslen(tag);
	for (int i = 0; i < len; i++){
		if (tag[i] != ','){
			temp[k] = tag[i];
			k++;
			if (i == len - 1){
				tempTags.push_back(temp);
			}
		}
		else{
			tempTags.push_back(temp);
			temp = new WCHAR[100];
			memset(temp, 0, 100);
			k = 0;
		}
	}
	return tempTags;
}

int findingNumOfNotesCorrespondedWith(NODENOTE *list){
	int count = 0;
	while (list != NULL){
		list = list->next;
		count++;
	}

	return count;
}

bool notExistInListView(WCHAR *list, vector<LVITEM*> &l){
	for (int i = 0; i < l.size(); i++){
		if (wcscmp(l[i]->pszText, list) == 0)
			return false;
	}
	return true;
}

bool existInListNote(WCHAR *tag){
	for (int i = 0; i < MAX_SIZE_HASHTABLE; i++){
		if (wcscmp(listnote[i]->tag, tag) == 0)
			return true;
	}
	return false;
}

void addNumOfNoteCorrespondedWith(HWND &h, NODENOTE *list, int i){
	WCHAR num[100];
	wsprintf(num, L"%d", findingNumOfNotesCorrespondedWith(list));
	ListView_SetItemText(h, i, 1, num);
}

void addTagToListView(vector<LVITEM*> &l, HWND &h, WCHAR *tag, WCHAR *n){

	LVITEM *lv = new LVITEM();
	lv->mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
	lv->iSubItem = 0;
	lv->pszText = tag;
	lv->iImage = 0;
	lv->cchTextMax = 100;
	//lvi.pszText = listtag[i];
	l.push_back(lv);


	ListView_InsertItem(h, lv);

}

void insertListViewColumn(HWND &h){
	LVCOLUMN lvCol;

	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_CENTER | LVCF_WIDTH;

	lvCol.cx = 100;
	lvCol.pszText = _T("Name Tag");
	ListView_InsertColumn(h, 0, &lvCol);

	lvCol.fmt = LVCFMT_CENTER | LVCF_WIDTH;
	lvCol.cx = 250;
	lvCol.pszText = _T("Number of Notes corresponded with");
	ListView_InsertColumn(h, 1, &lvCol);

}

void insertListViewContentColumn(HWND &h){
	LVCOLUMN lvCol;

	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_CENTER;
	lvCol.cx = 350;
	lvCol.pszText = _T("Notes Content");
	ListView_InsertColumn(h, 0, &lvCol);
}

void addBriefContentToListView(NODENOTE *l, vector<LVITEM*> &lvv, HWND &h){
	cnt.clear();//xóa vùng nhớ cũ
	lvv.clear();//xóa vùng nhớ cũ
	ListView_DeleteAllItems(h);
	WCHAR *content = NULL;

	while (l != NULL){
		
		if (wcslen(l->note) <= 50){
			content = new WCHAR[wcslen(l->note) + 1];//xuống dòng
			content[0] = '\0';
			wcscat(content, l->note);
			wcscat(content, L"\n");
			cnt.push_back(content);
		}
		else{
			content = new WCHAR[55];
			content[0] = '\0';
			for (int i = 0; i < 50; i++){
				content[i] = l->note[i];
			}
			content[50] = '\0';
			wcscat(content, L"...\n");
			cnt.push_back(content);
		}
		l = l->next;
	}

	for (int i = 0; i < cnt.size(); i++){
		LVITEM *lv = new LVITEM();
		lv->mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
		lv->iSubItem = 0;
		lv->pszText = cnt[i];
		lv->iImage = 0;
		lv->cchTextMax = 100;
		//lvi.pszText = listtag[i];
		lvv.push_back(lv);

		ListView_InsertItem(h, lv);
		
	}
}

void showFullContent(HWND &h, NODENOTE *listnote, WCHAR *c){
	if (wcslen(c) > 50){
		c[50] = '\0';
	}
	while (listnote != NULL){
		for (int i = 0; i < wcslen(c); i++){
			if (listnote->note[i] != c[i])
				break;
			if (i == wcslen(c) - 1){
				MessageBox(h, listnote->note, L"Full Content", 0);
				return;
			}
		}
		listnote = listnote->next;
	}
}

int sumOfNotes(){
	int sum = 0;
	
	for (int i = 0; i < lvi.size(); i++){
		int index = searchTag(lvi[i]->pszText);
		sum += findingNumOfNotesCorrespondedWith(listnote[index]);
	}
	
	return sum;
}

float setSizeForTag(NODENOTE *n, int sum){
	int count = findingNumOfNotesCorrespondedWith(n);
	
	float percent = (float)(count * 100 / sum);

	return percent / 2;
}

void drawTagCloud(HWND hCloud, HDC hdc){
	HWND *tagcloud = NULL;
	float size = 0;
	int index = 0;
	int count = 0;
	int x = 10, y = 20, xnextMax = 0;

	int sum = sumOfNotes();//tổng số note hiện có trong các tag

	for (int i = 0; i < lvi.size(); i++){

		if (count > 20){
			x += xnextMax;
			y = 10;
			count = 0;
		}

		index = searchTag(lvi[i]->pszText);
		size = setSizeForTag(listnote[index], sum);

		INITCOMMONCONTROLSEX icc;
		icc.dwSize = sizeof(icc);
		icc.dwICC = ICC_WIN95_CLASSES;
		InitCommonControlsEx(&icc);

		GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
		hFont = CreateFont(size + 10, size + 5,
			lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
			lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
			lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
			lf.lfPitchAndFamily, lf.lfFaceName);

		tagcloud = new HWND();

		if (size < 5){
			size += 5;
			*tagcloud = CreateWindow(WC_STATICW, lvi[i]->pszText, WS_CHILD | WS_VISIBLE, x, y,
				wcslen(lvi[i]->pszText) * 15 + size, size * 2, hCloud, NULL, hInst, NULL);
			y += size + 8;
			if (xnextMax < wcslen(lvi[i]->pszText) * 15 + size)
				xnextMax = wcslen(lvi[i]->pszText) * 15 + size;
		}
		else{
			*tagcloud = CreateWindow(WC_STATICW, lvi[i]->pszText, WS_CHILD | WS_VISIBLE, x, y,
				wcslen(lvi[i]->pszText) * 15 + size, size + 10, hCloud, NULL, hInst, NULL);
			y += size + 12;
			if (xnextMax < wcslen(lvi[i]->pszText) * 15 + size)
				xnextMax = wcslen(lvi[i]->pszText) * 15 + size;
		}
		SendMessage(*tagcloud, WM_SETFONT, WPARAM(hFont), TRUE);

		tc.push_back(tagcloud);
		count++;
		
	}
}

WCHAR* brwsNote(WCHAR *note){
	WCHAR result[MAX_SIZE_HASHTABLE];
	WCHAR w[6];
	memset(result, 0, MAX_SIZE_HASHTABLE);
	memset(w, 0, 6);
	for (int i = 0; i < MAX_SIZE_HASHTABLE; i++){
		if (wcscmp(listnote[i]->tag, NULLKEY) != 0){
			NODENOTE *temp = listnote[i]->next;

			while (temp != NULL && wcscmp(temp->note, note) != 0){
				temp = temp->next;
			}
			if (temp != NULL && wcscmp(temp->note, note) == 0){
				wcscat(result, listnote[i]->tag);//trả về tag chứa note cần tìm
				wcscat(result, L",");
			}
		}
	}
	return result;
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd = NULL;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   viewTag = CreateWindow(WC_LISTVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_REPORT | WS_BORDER
	   , 10, 10, 350, 450, hWnd, NULL, hInst, NULL);

   viewContent = CreateWindow(WC_LISTVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_REPORT | WS_BORDER
	   , 360, 10, 350, 450, hWnd, NULL, hInst, NULL);

   insertListViewColumn(viewTag);
   insertListViewContentColumn(viewContent);

   listnote = readData();//đọc dữ liệu từ file

   CreateWindow(WC_BUTTONW, L"Xem biểu đồ", WS_CHILD | WS_VISIBLE | WS_EX_TRANSPARENT,
	   730, 400, 100, 30, hWnd, (HMENU)IDC_TAGCLOUD, hInst, NULL);

   CreateWindow(WC_BUTTON, L"Tìm Note", WS_CHILD | WS_VISIBLE | WS_EX_TRANSPARENT,
	   730, 50, 120, 30, hWnd, (HMENU)IDC_BROWSENOTE, hInst, NULL);

   CreateWindow(WC_BUTTON, L"Tìm Tag", WS_CHILD | WS_VISIBLE | WS_EX_TRANSPARENT,
	   870, 50, 120, 30, hWnd, (HMENU)IDC_BROWSETAG, hInst, NULL);

   CreateWindow(WC_BUTTON, L"Thêm ghi chú", WS_CHILD | WS_VISIBLE | WS_EX_TRANSPARENT,
	   780, 160, 150, 30, hWnd, (HMENU)IDC_ADDNOTE, hInst, NULL);

   CreateWindow(WC_BUTTON, L"Xóa ghi chú", WS_CHILD | WS_VISIBLE | WS_EX_TRANSPARENT,
	   780, 270, 150, 30, hWnd, (HMENU)IDC_DELETENOTE, hInst, NULL);

   CreateWindow(WC_BUTTON, L"Xem tất cả các ghi chú", WS_CHILD | WS_VISIBLE | WS_EX_TRANSPARENT,
	   845, 400, 150, 30, hWnd, (HMENU)IDC_VIEWALLNOTES, hInst, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_NOTIFY:{
					   if ((((LPNMHDR)lParam)->hwndFrom) == viewTag){
						   switch (((LPNMHDR)lParam)->code){
						   case NM_RCLICK:

							   GetCursorPos(&point);
							   hPopupMenu = CreatePopupMenu();
							   InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING | MF_UNCHECKED, ID_EXIT, L"Exit");
							   InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING | MF_UNCHECKED, ID_VIEWSTATISTICS, L"View Statistics");
							   InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING | MF_UNCHECKED, ID_VIEWNOTE, L"View Notes");
							   InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING | MF_UNCHECKED, IDC_REFRESH, L"Refresh");

							   TrackPopupMenu(hPopupMenu, TPM_BOTTOMALIGN | TPM_LEFTALIGN, point.x, point.y, 0, hWnd, NULL);
							   break;
						   case NM_CLICK:
							   
							   int num = ListView_GetNextItem(viewTag, -1, LVNI_FOCUSED);
							   
							   if (num != -1){
								   int index = searchTag(lvi[lvi.size() - num - 1]->pszText);

								   wcscpy(tagContent, listnote[index]->tag);

								   addBriefContentToListView(listnote[index]->next, lviContent, viewContent);
							   }
							   
							   break;
						   }
					   }

					   if ((((LPNMHDR)lParam)->hwndFrom) == viewContent){
						   switch (((LPNMHDR)lParam)->code){
						   case NM_RCLICK:

							   GetCursorPos(&point);
							   hPopupMenu = CreatePopupMenu();
							   InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING | MF_UNCHECKED, ID_EXIT, L"Exit");
							   InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING | MF_UNCHECKED, ID_VIEWSTATISTICS, L"View Statistics");
							   InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING | MF_UNCHECKED, ID_VIEWNOTE, L"View Notes");
							   InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING | MF_UNCHECKED, IDC_REFRESH, L"Refresh");

							   TrackPopupMenu(hPopupMenu, TPM_BOTTOMALIGN | TPM_LEFTALIGN, point.x, point.y, 0, hWnd, NULL);
							   break;
						   case NM_CLICK:{
											 int numContent = ListView_GetNextItem(viewContent, -1, LVNI_FOCUSED);
											 if (numContent != -1){
												 int index = searchTag(tagContent);
												 lviContent[lviContent.size() - numContent - 1]->pszText[wcslen(lviContent[lviContent.size() - numContent - 1]->pszText) - 1] = L'\0';
												 showFullContent(viewContent, listnote[index]->next, lviContent[lviContent.size() - numContent - 1]->pszText);
											 }
											 break;
						   }
						   }
					   }
					   break;
	}
	case WM_CREATE:{
					   INITCOMMONCONTROLSEX icc;
					   icc.dwSize = sizeof(icc);
					   icc.dwICC = ICC_WIN95_CLASSES;
					   InitCommonControlsEx(&icc);

					   GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
					   hFont = CreateFont(lf.lfHeight, lf.lfWidth,
						   lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
						   lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
						   lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
						   lf.lfPitchAndFamily, lf.lfFaceName);

					   SendMessage(hWnd, WM_SETFONT, WPARAM(hFont), TRUE);
					   break;
	}
	case WM_RBUTTONUP:{
						  point.x = GET_X_LPARAM(lParam);
						  point.y = GET_Y_LPARAM(lParam);

						  hPopupMenu = CreatePopupMenu();
						  InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING | MF_UNCHECKED, ID_EXIT, L"Exit");
						  InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING | MF_UNCHECKED, ID_VIEWSTATISTICS, L"View Statistics");
						  InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING | MF_UNCHECKED, ID_VIEWNOTE, L"View Notes");
						  InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING | MF_UNCHECKED, IDC_REFRESH, L"Refresh");

						  ClientToScreen(hWnd, &point);

						  TrackPopupMenu(hPopupMenu, TPM_BOTTOMALIGN | TPM_LEFTALIGN, point.x, point.y, 0, hWnd, NULL);
							break;
	}
	case WM_KEYDOWN:{
						if (wParam == 'A' && GetAsyncKeyState(VK_SHIFT) < 0){
							DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, (DLGPROC)QuickNote);
						}
						break;
	}
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_VIEWNOTE:{
							 for (int i = 0; i < MAX_SIZE_HASHTABLE; i++){
								 if (wcscmp(listnote[i]->tag, NULLKEY) != 0){
									 if (notExistInListView(listnote[i]->tag, lvi))
										 addTagToListView(lvi, viewTag, listnote[i]->tag, 0);

									 int num = 0;
									 for (int j = 0; j < lvi.size(); j++){//tìm vị trí của tag trong listview để insert
										 //số node có tag đó vào cột Number Of Node Corresponded With
										 if (wcscmp(listnote[i]->tag, lvi[j]->pszText) == 0){
											 num = lvi.size() - j - 1;
											 break;
										 }
									 }
									 addNumOfNoteCorrespondedWith(viewTag, listnote[i]->next, num);//node đầu chứa tag nên ko tính
								 }
							 }
							 break;
		}
		case ID_VIEWSTATISTICS:{
								   DialogBox(hInst, MAKEINTRESOURCE(IDD_TAGCLOUD), hWnd, (DLGPROC)TagCloud);
								   break;
		}
		case IDC_TAGCLOUD:{
							  DialogBox(hInst, MAKEINTRESOURCE(IDD_TAGCLOUD), hWnd, (DLGPROC)TagCloud);
							  break;
		}
		case IDC_BROWSENOTE:{
								DialogBox(hInst, MAKEINTRESOURCE(IDD_BROWSENOTE), hWnd, (DLGPROC)browseNote);
								break;
		}
		case IDC_BROWSETAG:{
							   DialogBox(hInst, MAKEINTRESOURCE(IDD_BROWSETAG), hWnd, (DLGPROC)browseTag);
							   break;
		}
		case IDC_ADDNOTE:{
							 DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, (DLGPROC)QuickNote);
							 break;
		}
		case IDC_VIEWALLNOTES:{
								  for (int i = 0; i < MAX_SIZE_HASHTABLE; i++){
									  if (wcscmp(listnote[i]->tag, NULLKEY) != 0){
										  if (notExistInListView(listnote[i]->tag, lvi))
											  addTagToListView(lvi, viewTag, listnote[i]->tag, 0);

										  int num = 0;
										  for (int j = 0; j < lvi.size(); j++){//tìm vị trí của tag trong listview để insert
											  //số node có tag đó vào cột Number Of Node Corresponded With
											  if (wcscmp(listnote[i]->tag, lvi[j]->pszText) == 0){
												  num = lvi.size() - j - 1;
												  break;
											  }
										  }
										  addNumOfNoteCorrespondedWith(viewTag, listnote[i]->next, num);//node đầu chứa tag nên ko tính
									  }
								  }
								  break;
		}
		case IDC_DELETENOTE:{
								int num = ListView_GetNextItem(viewTag, -1, LVNI_FOCUSED);
								ListView_DeleteItem(viewTag, num);// xóa trong list view đang hiển thị
								
								if (num != -1){
									int index = searchTag(lvi[lvi.size() - num - 1]->pszText);
									wcscpy(listnote[index]->tag, NULLKEY);

									//xóa tất cả các note liên quan
									NODENOTE *temp1 = listnote[index]->next;// header giữ lại để dùng tiếp
									NODENOTE *temp2 = listnote[index];

									while (temp1 != NULL){
										temp1 = temp1->next;
										delete temp2->next;
										temp2->next = temp1;
									}

									int realnum = lvi.size() - num - 1;//vị trí bị ngược
									lvi.erase(lvi.begin() + realnum);
									ListView_DeleteAllItems(viewContent);
									writeData();
								}
								break;
		}
		case IDC_REFRESH:{
							 ListView_DeleteAllItems(viewTag);
							 ListView_DeleteAllItems(viewContent);
							 for (int i = 0; i < lvi.size(); i++)
							 if (lvi[i] != NULL)
								 delete lvi[i];
							 lvi.clear();

							 for (int i = 0; i < lviContent.size(); i++)
							 if (lviContent[i] != NULL)
								 delete lviContent[i];
							 lviContent.clear();

							 break;
		}
		case ID_EXIT:{
						 DestroyWindow(hWnd);
						 break;
		}
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:{
					  hdc = BeginPaint(hWnd, &ps);
					  // TODO: Add any drawing code here...
					  EndPaint(hWnd, &ps);
					  break;
	}
	case WM_DESTROY:
		for (int i = 0; i < tc.size(); i++){
			delete tc[i];
		}
		tc.clear();

		for(int i = 0;i < lvi.size();i ++)
			delete lvi[i];
		lvi.clear();

		for(int i = 0;i < lviContent.size();i ++)
			delete lviContent[i];
		lviContent.clear();

		cnt.clear();

		for (int i = 0; i < MAX_SIZE_HASHTABLE; i++){
			if (wcscmp(listnote[i]->tag, NULLKEY) != 0){
				NODENOTE *temp = NULL;

				while (listnote[i] != NULL){
					temp = listnote[i];
					listnote[i] = listnote[i]->next;
					delete temp;
				}
			}
			else
				delete listnote[i];
		}

		GdiplusShutdown(gdiplusToken);
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

INT_PTR CALLBACK QuickNote(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam){
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		int id = LOWORD(wParam);
		switch (id){
		case IDC_ADD:{
						 WCHAR note[1000], tag[1000];
						 memset(note, 0, 1000);
						 memset(tag, 0, 1000);
						 int lenn = GetDlgItemText(hDlg, IDC_NOTE, note, 999);
						 int lent = GetDlgItemText(hDlg, IDC_TAG, tag, 999);
						 if (lenn > 0 && lent > 0){

							 vector<WCHAR*> temp = seperatingTags(tag);

							 for (int i = 0; i < temp.size(); i++){
								 int index = insertTagAndNote(temp[i]); //kèm theo insert tag vào đầu các note		// băm
								 

								 if (index != -1){
									 NODENOTE *t = listnote[index]; //chèn nội dung note vào một danh sách liên kết
																	//nơi mà tại head node lưu trữ tag liên quan đến note đó

									 while (t->next != NULL){
										 t = t->next;
									 }
									 t->next = new NODENOTE();
									 wcscpy(t->next->note, note);
								 }
								 else
									 MessageBox(0, L"Hash Table đã đầy", L"Đủ dữ liệu", 0);

								 if (notExistInListView(listnote[index]->tag, lvi))
									 addTagToListView(lvi, viewTag, temp[i], 0);

								 int num = 0;
								 for (int j = 0; j < lvi.size(); j++){//tìm vị trí của tag trong listview để insert
																	  //số node có tag đó vào cột Number Of Node Corresponded With
									 if (wcscmp(temp[i], lvi[j]->pszText) == 0){
										 num = lvi.size() - j - 1;
										 break;
									 }
								 }
								 addNumOfNoteCorrespondedWith(viewTag, listnote[index]->next, num);//node đầu chứa tag nên ko tính

								 writeData();
							 }

							 EndDialog(hDlg, LOWORD(wParam));
						 }
						 else{
							 if (lenn <= 0 && lent > 0){
								 MessageBox(hDlg, L"Nội dung Note không được để trống", L"Thiếu dữ liệu", 0);
							 }
							 else if (lent <= 0 && lenn > 0)
								MessageBox(hDlg, L"Nội dung Tag không được để trống", L"Thiếu dữ liệu", 0);
							 else
								 MessageBox(hDlg, L"Nội dung Note và Tag không được để trống", L"Thiếu dữ liệu", 0);
						 }
						
 
						 break;
		}
		case IDCANCEL:
			EndDialog(hDlg, LOWORD(wParam));
			break;
		case IDC_CANCEL:
			EndDialog(hDlg, LOWORD(wParam));
			break;
		}
	}

	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK TagCloud(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam){

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:{
						   return (INT_PTR)TRUE;
	}

	case WM_COMMAND:{
						int id = LOWORD(wParam);
						switch (id){
						case IDCANCEL:
							EndDialog(hDlg, LOWORD(wParam));
							break;
						}
						break;
	}
	case WM_PAINT:{
					  PAINTSTRUCT ps;
					  HDC hdc;
					  hdc = BeginPaint(hDlg, &ps);
					
					  graphics = new Graphics(hdc);

					  drawTagCloud(hDlg, hdc);

					  delete graphics;
					  EndPaint(hDlg, &ps);
					  break;
	}
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK browseNote(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam){
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:{
						   return (INT_PTR)TRUE;
	}

	case WM_COMMAND:{
						int id = LOWORD(wParam);
						switch (id){
						case IDCANCEL:
							EndDialog(hDlg, LOWORD(wParam));
							break;
						case IDFIND:{
										WCHAR note[1000], result[1000];
										memset(note, 0, 1000);
										SetDlgItemText(hDlg, IDC_STATICTAGS, L"");
										//lấy nội dung từ edit box
										GetDlgItemText(hDlg, IDC_EDITNOTE, note, 999);

										if (wcscmp(note, L"") != 0){

											wcscpy(result, brwsNote(note));//tìm note và trả về các tag liên quan nếu tìm thấy

											if (wcscmp(result, L"") == 0){
												SetDlgItemText(hDlg, IDC_RESULT, L"Không tìm thấy");
											}
											else{
												SetDlgItemText(hDlg, IDC_STATICTAGS, result);
												SetDlgItemText(hDlg, IDC_RESULT, L"Đã tìm thấy");
											}
										}
										else{
											SetDlgItemText(hDlg, IDC_RESULT, L"Hãy nhập note cần tìm");
										}
										break;
						}
						}
						break;
	}
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK browseTag(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam){
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_NOTIFY:{
					   if ((((LPNMHDR)lParam)->hwndFrom) == viewTagBrowsed){
						   switch (((LPNMHDR)lParam)->code){
						   case NM_CLICK:{

											 int num = ListView_GetNextItem(viewTagBrowsed, -1, LVNI_FOCUSED);

											 if (num != -1){
												 int index = searchTag(lviBrowsed[lviBrowsed.size() - num - 1]->pszText);

												 wcscpy(tagContent, listnote[index]->tag);

												 addBriefContentToListView(listnote[index]->next, lviContentBrowsed, viewContentBrowsed);
											 }

											 break;
						   }
						   }
					   }

					   if ((((LPNMHDR)lParam)->hwndFrom) == viewContentBrowsed){
						   switch (((LPNMHDR)lParam)->code){
						   case NM_CLICK:{
											 int numContent = ListView_GetNextItem(viewContentBrowsed, -1, LVNI_FOCUSED);
											 if (numContent != -1){
												 int index = searchTag(tagContent);
												 lviContentBrowsed[lviContentBrowsed.size() - numContent - 1]->pszText[wcslen(lviContentBrowsed[lviContentBrowsed.size() - numContent - 1]->pszText) - 1] = L'\0';
												 showFullContent(viewContentBrowsed, listnote[index]->next, lviContentBrowsed[lviContentBrowsed.size() - numContent - 1]->pszText);
											 }
											 break;
						   }
						   }
					   }
					   break;
	}
	case WM_INITDIALOG:{
						   viewTagBrowsed = CreateWindow(WC_LISTVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_REPORT | WS_BORDER
							   , 10, 100, 350, 250, hDlg, NULL, hInst, NULL);

						   viewContentBrowsed = CreateWindow(WC_LISTVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_REPORT | WS_BORDER
							   , 360, 100, 343, 250, hDlg, NULL, hInst, NULL);

						   insertListViewColumn(viewTagBrowsed);
						   insertListViewContentColumn(viewContentBrowsed);

						   return (INT_PTR)TRUE;
	}

	case WM_COMMAND:{
						int id = LOWORD(wParam);
						switch (id){
						case IDCANCEL:
							for(int i = 0; i< lviBrowsed.size();i ++)
								delete lviBrowsed[i];
							lviBrowsed.clear();

							for(int i = 0;i < lviContentBrowsed.size();i ++)
								delete lviContentBrowsed[i];
							lviContentBrowsed.clear();
							EndDialog(hDlg, LOWORD(wParam));
							break;
						case IDFINDTAG:{
										   ListView_DeleteAllItems(viewTagBrowsed);
										   lviBrowsed.clear();
										   WCHAR tags[1000];
										   memset(tags, 0, 1000);

										   GetDlgItemTextW(hDlg, IDC_EDITTAGS, tags, 999);

										   if (wcscmp(tags, L"") == 0){
											   MessageBox(NULL, L"Hãy nhập nội dung tags cần tìm", L"Lỗi", NULL);
										   }
										   else{
											   WCHAR ntf[1000];
											   memset(ntf, 0, 1000);
											   vector<WCHAR*> temp = seperatingTags(tags);

											   for (int i = 0; i < temp.size(); i++){
												   if (existInListNote(temp[i])){
													   int index = searchTag(temp[i]);

													
														if (notExistInListView(listnote[index]->tag, lviBrowsed))
															   addTagToListView(lviBrowsed, viewTagBrowsed, temp[i], 0);

														int num = 0;
														for (int j = 0; j < lviBrowsed.size(); j++){//tìm vị trí của tag trong listview để insert
																								//số node có tag đó vào cột Number Of Node Corresponded With
															if (wcscmp(temp[i], lviBrowsed[j]->pszText) == 0){
																num = lviBrowsed.size() - j - 1;
																break;
															}
														}
														addNumOfNoteCorrespondedWith(viewTagBrowsed, listnote[index]->next, num);//node đầu chứa tag nên ko tính
													}
												   else{
														   wcscpy(ntf, L"Không tìm thấy tag ");
														   wcscat(ntf, temp[i]);
														   MessageBox(NULL, ntf, L"Thông báo", NULL);
														   ntf[0] = '\0';
													   }
												}
										   }
										   break;
						}	
						}
						break;
	}
	}
return (INT_PTR)FALSE;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}